const express = require("express")
const router = express.Router()
const Request = require("../models/request")

router.get('/', async (req, res) => {
    try {
        let request = Request.find()
        res.json({
            ok: true,
            message: "get requests",
            data: request
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.post("/", async (req, res) => {
    try {
        let request = await Request.find()

        Request.create(req.body, async (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Request created!",
                    element: request
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.delete("/:id", async (req, res) => {
    Request.findByIdAndDelete(req.params.id, async (error, data) => {
        if (error) {
            res.json({
                ok: false,
                messege: "Deleted shit!",
                el: data,
                error
            })
        } else {
            res.json({
                ok: true,
                message: 'Deleted',
                element: data,
            })
        }
    })
})
module.exports = router