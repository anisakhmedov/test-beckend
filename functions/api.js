const express = require('express');
const serverless = require('serverless-http');
const app = express();
const router = express.Router();

const bodyParser = require("body-parser");
const cors = require('cors')
const mongoose = require('mongoose')

app.use(cors())
app.use(bodyParser.json())

app.use("/request", require("../routes/request.js"))

mongoose.connect("mongodb://localhost:27017/app", () => {
    console.log("Успешно подключились к базе данных :)");
})

mongoose.connection.on('error', err => {
    console.log(err);
})

mongoose.set("debug", true)

//Get all students
router.get('/', (req, res) => {
  res.send('App is running..');
});

//Create new record
router.post('/add', (req, res) => {
  res.send('New record added.');
});

//delete existing record
router.delete('/', (req, res) => {
  res.send('Deleted existing record');
});

app.use('/.netlify/functions/api', router);
module.exports.handler = serverless(app);
