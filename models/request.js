const mongoose = require('mongoose')

const Request = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
    },
    disc: {
        type: String,
        required: true,
    },
})

module.exports = mongoose.model("Request", Request) 